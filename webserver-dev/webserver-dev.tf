
provider "aws" {
  region = "us-east-1"
}
module "ec2-module" {
    source = "../modules/ec2-module"
    instance_type = "t3a.micro"
    ami           = "ami-01cc34ab2709337aa"
    ssh_key       = "key-anand-13112021"
    instance_name = "dev-app-server"
    environment   = "Dev"
    product       = "WebApp"
}